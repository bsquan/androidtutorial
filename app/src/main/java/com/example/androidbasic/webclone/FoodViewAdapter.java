package com.example.androidbasic.webclone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.androidbasic.R;

import java.util.LinkedList;

public class FoodViewAdapter extends RecyclerView.Adapter<FoodViewAdapter.FoodViewHolder> {
    private final LinkedList<FoodData> foodList;
    private LayoutInflater layoutInflater;
    private ProgressBar progressBar;
    private Context context;

    public FoodViewAdapter(LinkedList<FoodData> foodList, LayoutInflater layoutInflater, ProgressBar progressBar) {
        this.foodList = foodList;
        this.layoutInflater = layoutInflater;
        this.progressBar = progressBar;
    }
    class FoodViewHolder extends RecyclerView.ViewHolder {

        private final TextView foodPrice;
        private final TextView foodName;
        private final ImageView foodImage;

        final FoodViewAdapter foodViewAdapter;

        public FoodViewHolder(@NonNull View itemView, FoodViewAdapter foodViewAdapter) {
            super(itemView);
            this.foodViewAdapter = foodViewAdapter;
            foodPrice = (TextView) itemView.findViewById(R.id.food_price);
            foodName = (TextView) itemView.findViewById(R.id.food_name);
            foodImage = (ImageView) itemView.findViewById(R.id.food_image);
        }
    }
    @NonNull
    @Override
    public FoodViewAdapter.FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View foodView = layoutInflater.inflate(R.layout.recyclerview_food,parent,false);
        context = parent.getContext();
        return new FoodViewHolder(foodView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewAdapter.FoodViewHolder holder, int position) {
        holder.foodName.setText(foodList.get(position).getFoodName());
        String price = foodList.get(position).getFoodPrice()+"€";
        holder.foodPrice.setText(price);
        Glide.with(context).load(foodList.get(position).getImagePath()).into(holder.foodImage);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

}
