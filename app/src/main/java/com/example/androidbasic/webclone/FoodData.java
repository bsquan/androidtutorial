package com.example.androidbasic.webclone;

public class FoodData {
    private String foodName;
    private String foodPrice;
    private String imagePath;

    public FoodData(String foodName, String foodPrice, String imagePath) {
        this.foodName = foodName;
        this.foodPrice = foodPrice;
        this.imagePath = imagePath;
    }

    public FoodData() {

    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        String base = "https://hqqrcode.api.developteam.net/hq-qrcode-api/v1/file/download";
        this.imagePath = base + imagePath;
    }
}
