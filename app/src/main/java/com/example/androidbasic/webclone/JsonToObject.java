package com.example.androidbasic.webclone;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class JsonToObject {
    private static final OkHttpClient client = new OkHttpClient();
    private LinkedList<FoodData> foodData;

    public JsonToObject(){
        foodData = new LinkedList<>();
    }

    public LinkedList<FoodData> getFoodData() {
        return foodData;
    }

    public void convert(){
        Request request = new Request.Builder()
                .url("https://hqqrcode.api.developteam.net/hq-qrcode-api/v1/food/list_by_restaurant?restaurantId=2924").get().build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray array  = jsonObject.getJSONArray("data");
                    for(int i =0;i<array.length();i++){
                        FoodData data = new FoodData();
                        data.setFoodName(array.getJSONObject(i).getString("name"));
                        data.setImagePath(array.getJSONObject(i).getString("imagePath"));
                        JSONObject price = new JSONObject(array.getJSONObject(0).getString("price"));
                        data.setFoodPrice(price.getJSONObject("qr_code").get("in_price").toString());
                        foodData.addLast(data);
                    }
                    System.out.println(array.getJSONObject(0).getString("name"));
                    JSONObject jsonObject2 = new JSONObject(array.getJSONObject(0).getString("price"));
                    System.out.println(jsonObject2.getJSONObject("qr_code").get("in_price").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
