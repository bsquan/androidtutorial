package com.example.androidbasic.webclone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidbasic.R;

public class FoodCard extends AppCompatActivity {
    private RecyclerView recyclerView;
    private FoodViewAdapter foodViewAdapter;
    private JsonToObject foodList;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_card);
        recyclerView = findViewById(R.id.recyclerview_food);
        foodList = new JsonToObject();
        foodList.convert();
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        foodViewAdapter = new FoodViewAdapter(foodList.getFoodData(), LayoutInflater.from(this),progressBar);
        recyclerView.setAdapter(foodViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
    }
}