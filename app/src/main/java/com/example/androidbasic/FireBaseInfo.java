package com.example.androidbasic;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FireBaseInfo extends AppCompatActivity {
    private TextView email;
    private TextView phone;
    private TextView name;
    private FirebaseUser firebaseUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fire_base_info);
        email = findViewById(R.id.firebase_user_email);
        phone = findViewById(R.id.firebase_user_phonenumber);
        name = findViewById(R.id.firebase_user_name);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        phone.setText(firebaseUser.getPhoneNumber());
        name.setText(firebaseUser.getDisplayName());
        email.setText(firebaseUser.getEmail());
        Intent intent = getIntent();
        if(intent.getData()!=null && firebaseUser!=null && !firebaseUser.isEmailVerified()){
            Uri uri = intent.getData();
            email.setText(firebaseUser.getEmail());

            firebaseAuth.applyActionCode(uri.getQueryParameter("oobCode")).addOnSuccessListener(this,new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    firebaseUser.reload();
                    email.setText(firebaseUser.getEmail());
                    Toast.makeText(FireBaseInfo.this, "Verify user successful.",
                            Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(FireBaseInfo.this, "Failed to verify user or user is verified.",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
        else {

            String iEmail = intent.getStringExtra("email");
            if (iEmail!=null){

                email.setText(iEmail);
            }
            String iName= intent.getStringExtra("name");
            if (iName!=null){

                name.setText(iName);
            }
        }


    }
}