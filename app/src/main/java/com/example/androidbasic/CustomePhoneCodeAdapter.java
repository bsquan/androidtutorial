package com.example.androidbasic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomePhoneCodeAdapter extends BaseAdapter {
    Context context;
    int[] flag;
    String[] countryName;
    LayoutInflater layoutInflater;

    CustomePhoneCodeAdapter(Context context, int[] flag, String[] countryName, LayoutInflater layoutInflater) {
        this.context=context;
        this.flag=flag;
        this.countryName=countryName;
        this.layoutInflater=layoutInflater;
    }

    @Override
    public int getCount() {
        return flag.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.country_code,null);
            ImageView imageView =(ImageView) convertView.findViewById(R.id.country_flag);
            TextView textView =(TextView) convertView.findViewById(R.id.country_code);
            imageView.setImageResource(flag[position]);

            textView.setText(countryName[position]);
        }
        return convertView;
    }
}
