package com.example.androidbasic;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class FirebaseLoginWithPhone extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner;
    EditText phone;
    int[] flag;
    String[] code;
    String selectedCode;
    String mVerificationId;
    private final String TAG = FirebaseLoginWithPhone.class.getSimpleName();
    private String mInputNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_login_with_phone);
        phone = findViewById(R.id.firebase_phone_input);
        spinner = findViewById(R.id.firebase_phone_spinner);
        TypedArray typedArray = getResources().obtainTypedArray(R.array.country_flag);
        flag = new int[typedArray.length()];
        for (int i=0;i<typedArray.length();i++){
            flag[i]=typedArray.getResourceId(i,0);
        }
        typedArray.recycle();
        code = getResources().getStringArray(R.array.country_code);
        spinner.setOnItemSelectedListener(this);
        CustomePhoneCodeAdapter customePhoneCodeAdapter = new CustomePhoneCodeAdapter(getApplicationContext(),flag,code, LayoutInflater.from(this));
        spinner.setAdapter(customePhoneCodeAdapter);
        spinner.setSelection(0);
    }


    public void phoneAuthentication(View view) {
        if (phone.getText()!=null){
            mInputNumber = selectedCode+" "+phone.getText();
            Log.d(TAG, "phoneAuthentication: "+mInputNumber);
        }
        else {
            return;
        }
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(FirebaseAuth.getInstance())
                .setPhoneNumber(mInputNumber)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                        Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);

                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {
                        Log.w(TAG, "onVerificationFailed", e);
                        Toast.makeText(FirebaseLoginWithPhone.this, "Failed to send verify code, please check your internet connection or phone number",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(s, forceResendingToken);
                        Log.d(TAG, "onCodeSent:" + s);
                        mVerificationId =s;
                        Intent intent = new Intent(FirebaseLoginWithPhone.this,VerificationCode.class);
                        intent.putExtra("VerificationId",s);
                        intent.putExtra("PhoneNumber",mInputNumber);
                        startActivity(intent);
                    }
                })
                .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedCode=code[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
