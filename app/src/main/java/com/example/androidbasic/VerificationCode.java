package com.example.androidbasic;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class VerificationCode extends AppCompatActivity {
    private final String TAG = VerificationCode.class.getSimpleName();
    private FirebaseAuth mAuth;
    private PhoneAuthCredential mCredential;
    private String mVerificationId;
    private String mPhoneNumber;
    private TextInputLayout mOne;
    private TextInputLayout mTwo;
    private TextInputLayout mThree;
    private TextView mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        mOne = findViewById(R.id.verification_code_one);
        mTwo = findViewById(R.id.verification_code_two);
        mThree = findViewById(R.id.verification_code_three);
        mAuth = FirebaseAuth.getInstance();
        if (getIntent().getExtras()!=null) {
            Bundle bundle = getIntent().getExtras();
            mVerificationId = bundle.getString("VerificationId");
            mPhoneNumber = bundle.getString("PhoneNumber");
        }
        mHelper = findViewById(R.id.verification_code_helper);
        mHelper.setText("Please type the verification code send to "+mPhoneNumber);
    }

    public void verifyCodeOnClick(View view) {

        if (mOne.getEditText().getText() == null || mTwo.getEditText().getText() == null || mThree.getEditText().getText() == null) {
            Toast.makeText(VerificationCode.this, "Failed to send verify code, please check your internet connection or phone number",
                    Toast.LENGTH_LONG).show();
            return;
        }
        String code = mOne.getEditText().getText().toString()
                + mTwo.getEditText().getText().toString()
                + mThree.getEditText().getText().toString();
        mCredential = PhoneAuthProvider.getCredential(mVerificationId, code);
        mAuth.signInWithCredential(mCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            startActivity(new Intent(VerificationCode.this, FireBaseInfo.class));
                        } else {
                                // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(VerificationCode.this, "The verification code entered was invalid",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }
}