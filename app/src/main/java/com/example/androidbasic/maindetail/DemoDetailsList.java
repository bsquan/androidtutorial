package com.example.androidbasic.maindetail;

import com.example.androidbasic.CustomMarker.CustomMarkerActivity;
import com.example.androidbasic.FirebaseLogin;
import com.example.androidbasic.FirebaseLoginWithPhone;
import com.example.androidbasic.ForgotPassword;
import com.example.androidbasic.JsonResponse;
import com.example.androidbasic.MapCenteredPoint;
import com.example.androidbasic.R;
import com.example.androidbasic.activityintent.IntentDemo;
import com.example.androidbasic.basiclocation.MyLocationDemoActivity;
import com.example.androidbasic.basicmap.BasicMapDemoActivity;
import com.example.androidbasic.googledev.ImplicitIntents;
import com.example.androidbasic.googledev.Receiver;
import com.example.androidbasic.homework.Assignment1;
import com.example.androidbasic.homework.ChipDemo;
import com.example.androidbasic.homework.ImageViewDemo;
import com.example.androidbasic.homework.RadioBtn;
import com.example.androidbasic.homework.SpinnerDemo;
import com.example.androidbasic.homework.SpinnerDemoV2;
import com.example.androidbasic.homework.WebViewDemo;
import com.example.androidbasic.login.Login;
import com.example.androidbasic.login.Register;
import com.example.androidbasic.recyclerview.GridRecyclerDemo;
import com.example.androidbasic.rxandroid.ChickenLevel;
import com.example.androidbasic.sqlite.SqlActivity;
import com.example.androidbasic.uitwo.UI2;
import com.example.androidbasic.webclone.FoodCard;

public final class DemoDetailsList {
    private DemoDetailsList() {

    }

    //    add list of activity
    public static final DemoDetails[] DEMOS = {new DemoDetails(R.string.basic_recycler_demo_label, R.string.basic_recycler_demo_description, GridRecyclerDemo.class),
            new DemoDetails(R.string.basic_map_demo_label, R.string.basic_map_demo_description, BasicMapDemoActivity.class),
            new DemoDetails(R.string.my_location_demo_label, R.string.my_location_demo_description, MyLocationDemoActivity.class),
            new DemoDetails(R.string.my_marker_label, R.string.my_marker_description, CustomMarkerActivity.class),
            new DemoDetails(R.string.my_intent_label, R.string.my_intent_description, IntentDemo.class),
            new DemoDetails(R.string.my_center_marker_label, R.string.my_center_marker_description, MapCenteredPoint.class),
            new DemoDetails(R.string.my_imageBtn_label, R.string.my_imageBtn_description, UI2.class),
            new DemoDetails(R.string.my_radioBtn_label, R.string.my_radioBtn_description, RadioBtn.class),
            new DemoDetails(R.string.my_chip_label, R.string.my_chip_description, ChipDemo.class),
            new DemoDetails(R.string.my_implicitIntent_label, R.string.my_implicitIntent_description, ImplicitIntents.class),
            new DemoDetails(R.string.my_implicitIntentReceiver_label, R.string.my_implicitIntentReceiver_description, Receiver.class),
            new DemoDetails(R.string.my_json_label, R.string.my_json_description, JsonResponse.class),
            new DemoDetails(R.string.my_homework_label, R.string.my_homework_description, Assignment1.class),
            new DemoDetails(R.string.my_webView_label, R.string.my_webView_description, WebViewDemo.class),
            new DemoDetails(R.string.my_ImageView_label, R.string.my_ImageView_description, ImageViewDemo.class),
            new DemoDetails(R.string.my_Spinner_label, R.string.my_Spinner_description, SpinnerDemo.class),
            new DemoDetails(R.string.my_SpinnerV2_label, R.string.my_SpinnerV2_description, SpinnerDemoV2.class),
            new DemoDetails(R.string.my_Login_label, R.string.my_Login_description, Login.class),
            new DemoDetails(R.string.my_sqlite_label, R.string.my_sqlite_description, SqlActivity.class),
            new DemoDetails(R.string.my_webClone_label,R.string.my_webClone_description, FoodCard.class),
            new DemoDetails(R.string.my_register_label,R.string.my_register_description, Register.class),
            new DemoDetails(R.string.my_rx_label,R.string.my_rx_description, ChickenLevel.class),
            new DemoDetails(R.string.my_firebase_label,R.string.my_firebase_description, FirebaseLogin.class),
            new DemoDetails(R.string.my_firebaseLoginPhone_label,R.string.my_firebaseLoginPhone_description, FirebaseLoginWithPhone.class),
            new DemoDetails(R.string.my_forgot_label,R.string.my_forgot_description, ForgotPassword.class)};
}
