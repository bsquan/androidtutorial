package com.example.androidbasic.maindetail;

import androidx.appcompat.app.AppCompatActivity;

public class DemoDetails {
    public final int titleId;
    public final int descriptionId;
    public final Class<? extends AppCompatActivity> activityClass;

    public DemoDetails(int titleId, int descriptionId, Class<? extends AppCompatActivity> activityClass) {
        this.titleId = titleId;
        this.descriptionId = descriptionId;
        this.activityClass = activityClass;
    }
}
