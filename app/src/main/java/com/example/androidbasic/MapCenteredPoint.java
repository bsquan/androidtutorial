package com.example.androidbasic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MapCenteredPoint extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener {

    private GoogleMap mMap;
    Marker point1;
    private Marker point2;
    private TextView address1;
    private TextView address2;
    Geocoder geocoder;
    private Address address;
    private Button confirm;
    private String location1;
    private String location2;
    Intent startIntent;
    GetFloatingIconClick receiver;
    IntentFilter filter = new IntentFilter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_centered_point);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        geocoder = new Geocoder(this);
        confirm = (Button) findViewById(R.id.confirm);
        address1 = findViewById(R.id.point1);
        address2 = findViewById(R.id.point2);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(10.849720, 106.771615);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Origin marker"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 17.0f));

        // these function glitched af, replace with image view provide better result
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        //
    }

    @Override
    public void onCameraIdle() {
        if (point1 == null) {
            point1 = mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).title("origin"));
            address1.setSelected(true);
        }
        if (point1 != null && point2 == null) {

            point1.setPosition(mMap.getCameraPosition().target);
            point1.setVisible(true);
            point1.setAnchor(0.5f, 1f);
//            try {
//                address = geocoder.getFromLocation(point1.getPosition().latitude, point1.getPosition().longitude, 1).get(0);
//                address1.setText("Origin: " + address.getAddressLine(0));
//                address1.setVisibility(View.VISIBLE);
//                confirm.setBackgroundColor(getResources().getColor(R.color.active));
////                Toast.makeText(this,address.getAddressLine(0),Toast.LENGTH_LONG).show();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            double lat=point1.getPosition().latitude;
            double lng=point1.getPosition().longitude;
            Observable.fromCallable(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    try {
                        return geocoder.getFromLocation(lat, lng, 1).get(0).getAddressLine(0);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    return "";
                }
            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(s -> {
                address1.setText("Origin: " + s);
                address1.setVisibility(View.VISIBLE);
                confirm.setBackgroundColor(getResources().getColor(R.color.active));
            });
        }
        if (point2 != null) {
            point2.setPosition(mMap.getCameraPosition().target);
            point2.setVisible(true);
            point2.setAnchor(0.5f, 1f);
            try {
                address = geocoder.getFromLocation(point2.getPosition().latitude, point2.getPosition().longitude, 1).get(0);
                address2.setText("Destination: " + address.getAddressLine(0));
                address2.setVisibility(View.VISIBLE);
                address2.setSelected(true);
                confirm.setBackgroundColor(getResources().getColor(R.color.active));
//                Toast.makeText(this,address.getAddressLine(0),Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onCameraMoveStarted(int i) {
        confirm.setBackgroundColor(getResources().getColor(R.color.passive));
    }

    @Override
    public void onCameraMove() {
        if (point1 != null && point2 == null) {
            point1.setPosition(mMap.getCameraPosition().target);
            point1.setAnchor(0.5f, 1.2f);
        } else {
            point2.setPosition(mMap.getCameraPosition().target);
            point2.setAnchor(0.5f, 1.2f);
        }
    }

    public void setDirection(View view) {
        if (point1 != null && point2 == null) {
            point2 = mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).title("destination"));
            String lat = Double.toString(point1.getPosition().latitude);
            String lng = Double.toString(point1.getPosition().longitude);
            location1 = lat + "," + lng;
            address1.setTextColor(getResources().getColor(R.color.active));
            return;
        }
        if (point1 != null && point2 != null) {
            String lat = Double.toString(point2.getPosition().latitude);
            String lng = Double.toString(point2.getPosition().longitude);
            location2 = lat + "," + lng;
            address2.setTextColor(getResources().getColor(R.color.active));
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("https://www.google.com/maps/dir/?api=1&origin=" + location1 + "&destination=" + location2 + "&travelmode=driving"));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
            startIntent = new Intent(MapCenteredPoint.this, FloatWidgetService.class);
            startService(startIntent);

        }

        // intent for current position

//        Uri gmmIntentUri = Uri.parse("google.navigation:q="+lat+","+lng);
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
//        startActivity(mapIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new GetFloatingIconClick();
        filter.addAction(FloatWidgetService.BROADCAST_ACTION);
        registerReceiver(receiver, filter);

    }

    private class GetFloatingIconClick extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent selfIntent = new Intent(MapCenteredPoint.this, MapCenteredPoint.class);
            selfIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(selfIntent);
        }
    }
}