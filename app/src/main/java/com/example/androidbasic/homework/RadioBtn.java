package com.example.androidbasic.homework;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.androidbasic.R;

public class RadioBtn extends AppCompatActivity {

    ConstraintLayout parent;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radiobtn);
        parent = (ConstraintLayout) findViewById(R.id.parent);
        text = (TextView) findViewById(R.id.textView2);
    }

    public void clickRed(View view) {
        parent.setBackgroundColor(getResources().getColor(R.color.red));
        text.setTextColor(getResources().getColor(R.color.blue));
    }

    public void clickBlue(View view) {
        parent.setBackgroundColor(getResources().getColor(R.color.blue));
        text.setTextColor(getResources().getColor(R.color.red));
    }

    public void clickYellow(View view) {
        parent.setBackgroundColor(getResources().getColor(R.color.yellow));
        text.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light));
    }

    public void clickOrange(View view) {
        parent.setBackgroundColor(getResources().getColor(R.color.orange));
        text.setTextColor(getResources().getColor(R.color.design_default_color_primary_dark));

    }
}