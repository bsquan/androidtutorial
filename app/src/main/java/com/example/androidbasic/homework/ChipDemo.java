package com.example.androidbasic.homework;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

public class ChipDemo extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chip);
        ChipGroup chipGroup = (ChipGroup) findViewById(R.id.chipGroup);
        chipGroup.setOnCheckedChangeListener((chipGroup1, i) -> {

            Chip chip = chipGroup1.findViewById(i);
            if (chip != null)
                Toast.makeText(getApplicationContext(), "Chip is " + chip.getChipText(), Toast.LENGTH_SHORT).show();


        });

        Chip chip = findViewById(R.id.chip);
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Close is Clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
