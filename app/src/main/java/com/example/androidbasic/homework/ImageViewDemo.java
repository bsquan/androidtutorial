package com.example.androidbasic.homework;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;

public class ImageViewDemo extends AppCompatActivity {
    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view_demo);
        imageView = findViewById(R.id.image);
        textView = findViewById(R.id.textview);

        findViewById(R.id.btn_center).setOnClickListener(clickListener);
        findViewById(R.id.btn_centerCrop).setOnClickListener(clickListener);
        findViewById(R.id.btn_centerInside).setOnClickListener(clickListener);
        findViewById(R.id.btn_fitCenter).setOnClickListener(clickListener);
        findViewById(R.id.btn_fitEnd).setOnClickListener(clickListener);
        findViewById(R.id.btn_fitStart).setOnClickListener(clickListener);
        findViewById(R.id.btn_fitXY).setOnClickListener(clickListener);
    }
    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ImageView.ScaleType scaletype = ImageView.ScaleType.CENTER;
            switch (view.getId())
            {
                case R.id.btn_center:
                    scaletype = ImageView.ScaleType.CENTER;
                    break;
                case R.id.btn_centerCrop:
                    scaletype = ImageView.ScaleType.CENTER_CROP;
                    break;
                case R.id.btn_centerInside:
                    scaletype = ImageView.ScaleType.CENTER_INSIDE;
                    break;
                case R.id.btn_fitCenter:
                    scaletype = ImageView.ScaleType.FIT_CENTER;
                    break;
                case R.id.btn_fitEnd:
                    scaletype = ImageView.ScaleType.FIT_END;
                    break;
                case R.id.btn_fitStart:
                    scaletype = ImageView.ScaleType.FIT_START;
                    break;
                case R.id.btn_fitXY:
                    scaletype = ImageView.ScaleType.FIT_XY;
                    break;

            }

            imageView.setScaleType(scaletype);
            textView.setText(((Button)view).getText());
        }
    };
}