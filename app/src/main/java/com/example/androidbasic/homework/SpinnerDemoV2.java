package com.example.androidbasic.homework;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;

public class SpinnerDemoV2 extends AppCompatActivity {
    String[] fruits={"Apple","Grapes","Mango"};
    int images[] = {R.mipmap.apple,R.mipmap.grapes, R.mipmap.mango};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_demo_v2);
        Spinner spin = (Spinner) findViewById(R.id.spinner_v2);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(SpinnerDemoV2.this, "You Select Position: "+position+" "+fruits[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerAdapter customAdapter = new SpinnerAdapter(getApplicationContext(),images,fruits);
        spin.setAdapter(customAdapter);

    }
}