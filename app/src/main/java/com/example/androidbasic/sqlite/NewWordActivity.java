package com.example.androidbasic.sqlite;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;

import static com.example.androidbasic.sqlite.SqlActivity.EXTRA_DATA_ID;
import static com.example.androidbasic.sqlite.SqlActivity.EXTRA_DATA_UPDATE_WORD;

public class NewWordActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.androidbasic.sqlite.REPLY";
    public static final String EXTRA_REPLY_ID = "com.example.androidbasic.sqlite.REPLY_ID";

    private EditText mEditWordView;
    private Bundle extras;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);

        mEditWordView = findViewById(R.id.edit_word);
        int id = -1 ;
        extras = getIntent().getExtras();

        // If we are passed content, fill it in for the user to edit.
        if (extras != null) {
            String word = extras.getString(EXTRA_DATA_UPDATE_WORD, "");
            if (!word.isEmpty()) {
                mEditWordView.setText(word);
                mEditWordView.setSelection(word.length());
                mEditWordView.requestFocus();
            }
        } // Otherwise, start with empty fields.
    }

    public void editWord(View view) {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(mEditWordView.getText())) {
            // No word was entered, set the result accordingly.
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            // Get the new word that the user entered.
            String word = mEditWordView.getText().toString();
            // Put the new word in the extras for the reply Intent.
            replyIntent.putExtra(EXTRA_REPLY, word);
            if (extras != null && extras.containsKey(EXTRA_DATA_ID)) {
                int id = extras.getInt(EXTRA_DATA_ID, -1);
                if (id != -1) {
                    replyIntent.putExtra(EXTRA_REPLY_ID, id);
                }
            }
            // Set the result status to indicate success.
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }
}