package com.example.androidbasic;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class JsonResponse extends AppCompatActivity {

    TextView textView;
    private final OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder()
            .url("https://api-sandbox.tiki.vn/integration/v1/categories")
            .build();
    JSONObject jsonObject;
    String temp="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_response);
        textView = findViewById(R.id.json_parse);
        temp = "url: "+ "https://api-sandbox.tiki.vn/integration/v1/categories\n";
    }

    public void parseJson(View view) throws JSONException {
        try (Response response = client.newCall(request).execute()){
            jsonObject = new JSONObject(response.body().string());
            temp+= "id: "+jsonObject.getJSONObject("meta").getString("id")+"\n";
            temp+= "source: "+jsonObject.getJSONObject("meta").getString("source")+"\n";
            textView.setText(temp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static class Holder{
        Map<String,Content> data;
    }
    static class Content{
        String content;
    }
}
