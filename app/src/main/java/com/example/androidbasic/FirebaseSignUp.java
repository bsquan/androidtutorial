package com.example.androidbasic;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseSignUp extends AppCompatActivity {
    private MaterialCheckBox materialCheckBox;
    private TextInputLayout email;
    private TextInputLayout password;
    private TextInputLayout firstName;
    private TextInputLayout lastName;
    private FirebaseAuth mAuth;
    private final String TAG = FirebaseSignUp.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_sign_up);
        materialCheckBox = findViewById(R.id.firebase_checkbox);
        email = findViewById(R.id.firebase_signup_email);
        password = findViewById(R.id.firebase_signup_password);
        firstName = findViewById(R.id.verification_code_one);
        lastName = findViewById(R.id.firebase_lastname);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("I agree with Terms & Condition");
        spannableStringBuilder.setSpan(new UnderlineSpan(), 13, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        materialCheckBox.setText(spannableStringBuilder);
    }

    public void firebaseSignUpClick(View view) {
// test sign up first
        mAuth.createUserWithEmailAndPassword(email.getEditText().getText().toString(), password.getEditText().getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            ActionCodeSettings actionCodeSettings = ActionCodeSettings.newBuilder()
//                                    .setHandleCodeInApp(true)
//                                    .setUrl("https://quickstart-1610416300521.firebaseapp.com/?email="+
//                                            username.getEditText().getText().toString())
//                                    .build();

                            if (user != null) {
                                user.sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d(TAG, "Email sent.");
                                                }
                                            }
                                        });
                            }
                            Intent intent = new Intent(FirebaseSignUp.this, FirebaseLogin.class);
                            startActivity(intent);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(FirebaseSignUp.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(FirebaseSignUp.this, "Error: failed to create a new account",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}