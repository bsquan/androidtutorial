package com.example.androidbasic.CustomMarker;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomMarkerActivity extends AppCompatActivity implements
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private final LatLng car1= new LatLng(10.850545, 106.771658);
    private final LatLng car2= new LatLng(10.851164, 106.772298);
    private final LatLng car3= new LatLng(10.850407, 106.772359);
    private final LatLng car4= new LatLng(10.850279, 106.773006);
    private final LatLng car5= new LatLng(10.850868, 106.771216);
    private final LatLng car6= new LatLng(10.849997, 106.772277);
    private final LatLng car7= new LatLng(10.849654, 106.772139);
    private final LatLng car8= new LatLng(10.849977, 106.771079);
    private final LatLng car9= new LatLng(10.850767, 106.770713);
    private final LatLng car10= new LatLng(10.850000, 106.773198);
    
    private Bitmap imgCar;

    private Marker mCar1;
    private Marker mCar2;
    private Marker mCar3;
    private Marker mCar4;
    private Marker mCar5;
    private Marker mCar6;
    private Marker mCar7;
    private Marker mCar8;
    private Marker mCar9;
    private Marker mCar10;
    private Marker school;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_mapper_demo);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        imgCar = scaleImage((BitmapDrawable)getResources().getDrawable(R.mipmap.car_foreground),0.5);
        mCar1 = googleMap.addMarker(new MarkerOptions().position(car1).flat(true).icon(BitmapDescriptorFactory
        .fromBitmap(imgCar)));
        mCar1.setTag("car 1 clicked");

        mCar2 = googleMap.addMarker(new MarkerOptions().position(car2).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar2.setTag("car 2 clicked");

        mCar3 = googleMap.addMarker(new MarkerOptions().position(car3).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar3.setTag("car 3 clicked");

        mCar4 = googleMap.addMarker(new MarkerOptions().position(car4).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar4.setTag("car 4 clicked");

        mCar5 = googleMap.addMarker(new MarkerOptions().position(car5).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar5.setTag("car 5 clicked");

        mCar6 = googleMap.addMarker(new MarkerOptions().position(car6).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar6.setTag("car 6 clicked");

        mCar7 = googleMap.addMarker(new MarkerOptions().position(car7).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar7.setTag("car 7 clicked");

        mCar8 = googleMap.addMarker(new MarkerOptions().position(car8).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar8.setTag("car 8 clicked");

        mCar9 = googleMap.addMarker(new MarkerOptions().position(car9).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar9.setTag("car 9 clicked");

        mCar10 = googleMap.addMarker(new MarkerOptions().position(car10).flat(true).icon(BitmapDescriptorFactory
                .fromBitmap(imgCar)));
        mCar10.setTag("car 10 clicked");

        LatLng pos = new LatLng(10.849720, 106.771615);
        school= googleMap.addMarker(new MarkerOptions().position(pos));
        school.setZIndex(1f);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,17.0f));

        googleMap.setOnMarkerClickListener(this);
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        String message = (String) marker.getTag();
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
        return false;
    }

    private Bitmap scaleImage(BitmapDrawable bitmapDrawable,double scale){
        Bitmap b = bitmapDrawable.getBitmap();
        Bitmap scaleMarker = Bitmap.createScaledBitmap(b,(int)(b.getWidth()*scale),(int)(b.getHeight()*scale),false );
        return scaleMarker;
    }
}
