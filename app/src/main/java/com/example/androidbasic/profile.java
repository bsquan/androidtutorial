package com.example.androidbasic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class profile extends AppCompatActivity {
    private final OkHttpClient client = new OkHttpClient();
    SharedPreferences sharedPref;
    private String token;
    private TextView fullname;
    private TextView email;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        fullname = (TextView) findViewById(R.id.fullname);
        email = (TextView) findViewById(R.id.email);
        sharedPref = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        token = sharedPref.getString(getString(R.string.token), "no token");
        Log.d("profile", token);
        try {
            load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void load() throws Exception {

        if (token == "no token") {
            return;
        }
        Request request = new Request.Builder()
                .url("https://iservice.api.developteam.net/iservice-api/v1/account/profile")
                .get()
                .addHeader("Authorization", "Bearer " + token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI(response);
                    }
                });

            }
        });
    }
    private void updateUI(Response response){
        try (ResponseBody responseBody = response.body()) {
            JSONObject object = new JSONObject(responseBody.string());
            String emailData = email.getText()+object.getJSONObject("data").getString("email");
            email.setText(emailData);
            String nameData = fullname.getText()+object.getJSONObject("data").getString("fullName");
            fullname.setText(nameData);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void resetToken(View view) {
        editor = sharedPref.edit();
        editor.putString(getString(R.string.token), "");
        editor.commit();
    }
}