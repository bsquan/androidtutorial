package com.example.androidbasic.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;
import com.example.androidbasic.profile;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class Login extends AppCompatActivity {
    private final OkHttpClient client = new OkHttpClient();
    private EditText username;
    private EditText password;
    private JSONObject object;
    private MediaType JSON;
    private RequestBody requestBody;
    CircularProgressIndicator indicator;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //init var
        object = new JSONObject();
        JSON = MediaType.parse("application/json; charset=utf-8");
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        indicator = (CircularProgressIndicator) findViewById(R.id.progress);
        sharedPref = getSharedPreferences(getString(R.string.token),Context.MODE_PRIVATE);
        if(sharedPref.getString(getString(R.string.token),"")!=""){
            startActivity(new Intent(this, profile.class));
        }
    }

    public void login(View view) throws Exception {
        indicator.setVisibility(View.VISIBLE);
        run();
    }
    private void run() throws Exception {
        if (username.getText() == null || password.getText() == null){
            return;
        }
        //craft body
        object.put("username",username.getText().toString());
        object.put("password",password.getText().toString());
        requestBody = RequestBody.create(object.toString(),JSON);

        Request request = new Request.Builder()
                .url("https://iservice.api.developteam.net/iservice-api/v1/account/login")
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String token = jsonObject.getJSONObject("data").getString("token");

                    //save token
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.token), token);
                    editor.commit();
                    startActivity(new Intent(getBaseContext(), profile.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}