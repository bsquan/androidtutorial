package com.example.androidbasic.login;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidbasic.R;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Locale;
import java.util.Objects;

public class Register extends AppCompatActivity {
    TextInputLayout gender;
    TextInputLayout birthDay;
    TextInputLayout username;
    TextInputLayout phone;
    TextInputLayout email;
    TextInputLayout password;
    TextInputLayout address;
    ImageView picture;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        gender = findViewById(R.id.register_gender);

        AutoCompleteTextView textView = findViewById(R.id.gender_items);
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(this, R.layout.gender_item, getResources().getStringArray(R.array.gender));
        textView.setAdapter(stringArrayAdapter);

        ArrayAdapter<String> regionArrayAdapter = new ArrayAdapter<>(this, R.layout.region_item,
                getResources().getStringArray(R.array.region));
        AutoCompleteTextView regionView = findViewById(R.id.register_region);


        regionView.setOnItemClickListener((parent, view, position, id) -> {
            if (regionArrayAdapter.getItem(position).equals("vn")) {
                Locale locale = new Locale("vi", "VN");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent intent = new Intent(Register.this, Register.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("LANGUAGE_CHANGE", "vi");
                startActivity(intent);
            } else {
                Locale locale = new Locale("en", "US");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent intent = new Intent(Register.this, Register.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("LANGUAGE_CHANGE", "en");
                startActivity(intent);
            }
        });
        regionView.setAdapter(regionArrayAdapter);
        Intent intent = getIntent();
        // receive intent
        if (intent != null) {
            String lang = intent.getStringExtra("LANGUAGE_CHANGE");
            if (lang != null) {
                if(lang.equals("vi")){
                    regionView.setText(regionArrayAdapter.getItem(0),false);
                }
                else {
                    regionView.setText(regionArrayAdapter.getItem(1),false);
                }
            }
        }

        birthDay = findViewById(R.id.register_birthday);
        username = findViewById(R.id.register_username);
        phone = findViewById(R.id.register_phone);
        email = findViewById(R.id.register_email);
        password = findViewById(R.id.register_password);
        address = findViewById(R.id.register_address);
        picture = findViewById(R.id.register_form);
        picture.setOnClickListener(v -> {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
            }
            catch (ActivityNotFoundException e){
                Log.e("REGISTER", "onCreate: ", e);
            }
        });

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Select date");
        MaterialDatePicker<Long> picker = builder.build();

        picker.addOnPositiveButtonClickListener(selection -> Objects.requireNonNull(birthDay.getEditText()).setText(picker.getHeaderText()));

        Objects.requireNonNull(birthDay.getEditText()).setOnClickListener(v -> picker.show(getSupportFragmentManager(), "Select date"));

        Objects.requireNonNull(email.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Toast.makeText(Register.this, email.getEditText().getText() + Objects.requireNonNull(email.getSuffixText()).toString(), Toast.LENGTH_LONG).show();
            }
        });

        Objects.requireNonNull(username.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (username.getEditText().getText().toString().equals("")) {
                    username.setError(getString(R.string.username_error));
                } else {
                    username.setError(null);
                }
            }
        });
        Objects.requireNonNull(phone.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (phone.getEditText().getText().toString().equals("")||phone.getEditText().getText().toString().length()<11) {
                    phone.setError(getString(R.string.phone_error));
                } else {
                    phone.setError(null);
                }
            }
        });
        Objects.requireNonNull(email.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (email.getEditText().getText().toString().equals("")) {
                    email.setError(getString(R.string.email_error));
                } else {
                    email.setError(null);
                }
            }
        });
        Objects.requireNonNull(password.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (password.getEditText().getText().toString().equals("")||password.getEditText().getText().toString().length()<8) {
                    password.setError(getString(R.string.password_error));
                } else {
                    password.setError(null);
                }
            }
        });
        Objects.requireNonNull(address.getEditText()).setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (address.getEditText().getText().toString().equals("")) {
                    address.setError(getString(R.string.address_error));
                } else {
                    address.setError(null);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_IMAGE_CAPTURE&&resultCode==RESULT_OK){
            Bundle extras = data.getExtras();
            Bitmap imageBitmap =(Bitmap) extras.get("data");
            picture.setImageBitmap(imageBitmap);
        }
    }
}