package com.example.androidbasic.recyclerview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidbasic.R;

import java.util.LinkedList;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {

    class WordViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView wordItemView;
        final WordListAdapter mAdapter;

        public WordViewHolder(View itemView, WordListAdapter adapter) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.cell);
            this.mAdapter = adapter;
//            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            int mPosition = getLayoutPosition();
//        String element = mWordList.get(mPosition);
//        mWordList.set(mPosition, "Clicked! " + element);
//        mAdapter.notifyDataSetChanged();
        }
    }

    private final LinkedList<Integer> mWordList;
    private LayoutInflater mInflater;

    public WordListAdapter(Context context, LinkedList<Integer> wordList) {
        mInflater = LayoutInflater.from(context);
        this.mWordList = wordList;
    }

    @Override
    public int getItemCount() {
        return mWordList.size();
    }

    @NonNull
    @Override
    public WordListAdapter.WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int currentWidth = parent.getWidth() / 3;
        int currentHeight = currentWidth;
        View mItemView = mInflater.inflate(R.layout.card_grid, parent, false);
        mItemView.setLayoutParams(new ViewGroup.LayoutParams(currentWidth, currentHeight));
        return new WordViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull WordListAdapter.WordViewHolder holder, int position) {
//        Integer mCurrent = mWordList.get(position);
//        holder.wordItemView.setText("#"+mCurrent.toString());
        if (holder.getLayoutPosition() % 2 == 0) {
            holder.wordItemView.setBackgroundColor(0xFF6200EE);
        }
        if (holder.getLayoutPosition() % 2 != 0) {
            holder.wordItemView.setBackgroundColor(0xFF000000);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer mPosition = holder.getLayoutPosition();
                Integer element = mWordList.get(mPosition);
                Log.d("MyApp", mPosition.toString());

            }
        });
    }
}
