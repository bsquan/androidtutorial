package com.example.androidbasic.recyclerview;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidbasic.R;

import java.util.LinkedList;

public class GridRecyclerDemo  extends AppCompatActivity {
    private final LinkedList<Integer> mWordlist = new LinkedList<>();
    private RecyclerView mRecyclerView;
    private WordListAdapter mAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);
        for (int i = 0; i < 20; i++) {
            mWordlist.addLast(i);
        }
        // Get a handle to the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerview);
// Create an adapter and supply the data to be displayed.
        mAdapter = new WordListAdapter(this, mWordlist);
// Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
// Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,3,GridLayoutManager.VERTICAL,false));
    }
    public void onClick(View view) {
        int wordListSize = mWordlist.size();
        // Add a new word to the wordList
        int size = mWordlist.size();
        for (int i=size; i < size+10;i++){
            mWordlist.addLast(i);
        }
        // Notify the adapter, that the data have changed
        mRecyclerView.getAdapter().notifyItemInserted(wordListSize);
        // Scoll to the bottom
        mRecyclerView.smoothScrollToPosition(wordListSize);

    }
    public void onReset(View view){
        mWordlist.subList(10,mWordlist.size()).clear();
        // Get a handle to the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerview);
// Create an adapter and supply the data to be displayed.
        mAdapter = new WordListAdapter(this, mWordlist);
// Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
// Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
    }
}
